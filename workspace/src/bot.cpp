#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main() {

	DatosMemCompartida *dat;
	struct stat st;
	bool run = true;
	
	int fd = open("src/lvl1bot", O_RDWR);
	if (fd==-1)
	{
		perror("ERROR AL ABRIR BOT.CPP");	//error en open
	}
	
	if (fstat (fd, &st) == -1)
		perror ("ERROR EN OBTENER TAMAÑO BOT.CPP");	//length fichero
	
	dat = (DatosMemCompartida*)mmap(NULL, st.st_size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
	
	dat->accion = 1;
	
	//cerramos
	close(fd);
	
	//acciones (0, 1, -1)
	
	while(run) {
	
		usleep(25000);
		if(dat->raqueta1.y1 > dat->esfera.centro.y + dat->esfera.radio) 
			dat->accion = -1;	//-1 --> abajo			
		else if(dat->raqueta1.y2 < dat->esfera.centro.y - dat->esfera.radio) 
			dat->accion = 1;	//1 --> arriba
		else 
			dat->accion = 0;	//0 --> nada
	}

	munmap(dat, st.st_size);	
	
}
