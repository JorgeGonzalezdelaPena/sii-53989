#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

int main ()
{

	int fd;
	char ch = 'a';
	
	if (mkfifo ("src/FIFO_LOGGER", 0777) != 0) //0=octal (7 ---> 111)
		return 2;

	fd = open ("src/FIFO_LOGGER", O_RDONLY, 0777);
	if (fd == -1)
	{
		perror ("ERROR AL ABRIR EL ARCHIVO LOG.CPP");
		return 1;
	} 
	while (read(fd, &ch, 1) == 1)
		write (1, &ch, 1);
		
		
	close(fd);
	unlink("src/FIFO_LOGGER");
	return 0;
}
